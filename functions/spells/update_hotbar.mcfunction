execute as @s[tag=ingame,gamemode=adventure,scores={class=1}] run function rogues-basis:characters/sparket/set_hotbar
execute as @s[tag=ingame,gamemode=adventure,scores={class=2}] run function rogues-basis:characters/vohelm/set_hotbar
execute as @s[tag=ingame,gamemode=adventure,scores={class=3}] run function rogues-basis:characters/emma/set_hotbar
execute as @s[tag=ingame,gamemode=adventure,scores={class=4}] run function rogues-basis:characters/pluto/set_hotbar
execute as @s[tag=ingame,gamemode=adventure,scores={class=5}] run function rogues-basis:characters/halt/set_hotbar
execute as @s[tag=ingame,gamemode=adventure,scores={class=6}] run function rogues-basis:characters/barbose/set_hotbar

clear @s[gamemode=!creative,tag=!vengeuse,nbt=!{Inventory:[{id:"minecraft:book",Slot:6b}]}] book
item replace entity @s[gamemode=!creative,tag=!vengeuse] hotbar.6 with minecraft:book{display:{Name:"[{\"text\":\"VENGENCE\",\"color\":\"red\",\"italic\":\"false\",\"bold\":\"true\"},{\"text\":\" [\",\"color\":\"white\",\"italic\":\"false\"},{\"text\":\"1 Use\",\"color\":\"gold\",\"italic\":\"false\"},{\"text\":\"] (15 seconds X2 Mana and Heal)\",\"color\":\"white\",\"italic\":\"false\"}]"}}
clear @s[gamemode=!creative,tag=vengeuse] book

execute as @s[tag=lobby] at @s run function rogues-basis:spells/manaupl
