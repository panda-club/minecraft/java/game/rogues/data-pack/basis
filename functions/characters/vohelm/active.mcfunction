execute as @s[scores={goldenap=1..}] at @s run function rogues-basis:characters/vohelm/spells/heal

execute as @s[scores={shield=1..}] at @s run function rogues-basis:characters/vohelm/active/shield

execute as @s[tag=!spiritworld] at @s run function rogues-basis:characters/vohelm/active/weapon

#execute as @s[tag=!spiritworld,scores={Mana=3..},nbt=!{Inventory:[{id:"minecraft:golden_apple",Slot:4b}]}] run item replace entity @s hotbar.4 with golden_apple{display:{Name:"[{\"text\":\"Sacred Berries\",\"color\":\"green\",\"italic\":\"false\",\"bold\":\"true\"},{\"text\":\" (\",\"color\":\"white\",\"italic\":\"false\"},{\"text\":\"Heal 4\",\"color\":\"red\",\"italic\":\"false\"},{\"text\":\" / \",\"color\":\"white\",\"italic\":\"false\"},{\"text\":\"Glow nearest enemy\",\"color\":\"gray\",\"italic\":\"false\"},{\"text\":\") [\",\"color\":\"white\",\"italic\":\"false\"},{\"text\":\"3 Mana\",\"color\":\"gold\",\"italic\":\"false\"},{\"text\":\"]\",\"color\":\"white\",\"italic\":\"false\"}]"}}
execute as @s[tag=!spiritworld,scores={Mana=5..},nbt=!{Inventory:[{id:"minecraft:golden_apple",Slot:4b}]}] run item replace entity @s hotbar.4 with golden_apple{display:{Name:"[{\"text\":\"Sacred Berries\",\"color\":\"green\",\"italic\":\"false\",\"bold\":\"true\"},{\"text\":\" (\",\"color\":\"white\",\"italic\":\"false\"},{\"text\":\"Heal 5\",\"color\":\"red\",\"italic\":\"false\"},{\"text\":\") [\",\"color\":\"white\",\"italic\":\"false\"},{\"text\":\"5 Mana\",\"color\":\"gold\",\"italic\":\"false\"},{\"text\":\"]\",\"color\":\"white\",\"italic\":\"false\"}]",Lore:['{"text":"Heal 5 health","color":"green","italic":false}','{"text":"4 seconds of +20% speed","color":"green","italic":false}']}}

attribute @s minecraft:generic.attack_damage base set 1

scoreboard players operation @e[tag=totem] playerID -= @s playerID
execute as @s at @s if entity @e[tag=totem,distance=..7,scores={playerID=0}] run effect give @s minecraft:jump_boost 2 1
execute as @s at @s if entity @e[tag=totem,distance=..7,scores={playerID=0}] run attribute @s[tag=wood] minecraft:generic.attack_damage base set 4
execute as @s at @s if entity @e[tag=totem,distance=..7,scores={playerID=0}] run execute as @e[tag=totem,distance=..8,scores={playerID=0}] at @s run particle dust 1.0 0.5 0.5 1.0 ~ ~1 ~ .3 .5 .3 0 2 force
execute as @s at @s if entity @e[tag=totem,distance=..7,scores={playerID=0}] run particle dust 1.0 0.5 0.5 1.0 ~ ~.2 ~ .2 .1 .2 0 2 force
execute as @s at @s if entity @e[tag=totem,distance=..7,scores={playerID=0}] run tag @s add totemdust
execute as @s at @s if entity @e[tag=totem,distance=..7,scores={playerID=0}] run scoreboard players set @s totemres 40
execute as @s at @s run execute as @e[tag=totem,distance=7..,scores={playerID=0}] at @s run scoreboard players set @e[distance=..4,name=web] move 1045
execute as @s at @s run execute as @e[tag=totem,distance=7..,scores={playerID=0}] at @s run summon creeper ~ ~ ~ {ignited:1,Fuse:0,ExplosionRadius:2}
execute as @s at @s run tp @e[tag=totem,distance=7..,scores={playerID=0}] ~ ~-1000 ~
scoreboard players operation @e[tag=totem] playerID += @s playerID

effect give @s[scores={totemres=0..39}] minecraft:resistance 1

tag @s remove totemres
tag @s[scores={totemres=0..39}] add totemres

scoreboard players remove @s[scores={totemres=0..}] totemres 1

scoreboard players remove @s[scores={shieldT=0..}] shieldT 1